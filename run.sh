#!/bin/bash

source .ci/config.sh

if [ $IS_SHELL_BUILD == true ]; then
  echo "Executing a shell build..."
  source .ci/dagger/shell/build.sh
else
  echo "Shell build not detected..."
fi
