#!/bin/bash

# get Go examples source code repository
SOURCE=$(dagger --debug query <<EOF | jq -r .git.branch.tree.id
{
  git(url:"$APP_REPO") {
    branch(name:"$APP_BRANCH") {
      tree {
        id
      }
    }
  }
}
EOF
)

# mount source code repository in golang container
# build Go binary
# export binary from container to host filesystem
BUILD_BACKEND=$(dagger --debug query <<EOF | jq -r .container.from.withDirectory.withWorkdir.withExec.file.export
{
  container {
    from(address:"$APP_BUILD_IMAGE:$APP_BUILD_IMAGE_TAG") {
      withDirectory(path: "/src", directory:"$SOURCE") {
        withWorkdir(path:"/src/app/backend") {
          withExec(args:["go", "build", "-o", "$APP_NAME-backend", "main.go"]) {
            file(path:"$APP_NAME-backend") {
              export(path:"output/$APP_NAME-backend")
            }
          }
        }
      }
    }
  }
}
EOF
)

BUILD_API=$(dagger --debug query <<EOF | jq -r .container.from.withDirectory.withWorkdir.withExec.file.export
{
  container {
    from(address:"$APP_BUILD_IMAGE:$APP_BUILD_IMAGE_TAG") {
      withDirectory(path: "/src", directory:"$SOURCE") {
        withWorkdir(path:"/src/app/api") {
          withExec(args:["go", "build", "-o", "$APP_NAME-api", "main.go"]) {
            file(path:"$APP_NAME-api") {
              export(path:"output/$APP_NAME-api")
            }
          }
        }
      }
    }
  }
}
EOF
)

BUILD_FRONTEND=$(dagger --debug query <<EOF | jq -r .container.from.withDirectory.withWorkdir.withExec.file.export
{
  container {
    from(address:"$APP_BUILD_IMAGE:$APP_BUILD_IMAGE_TAG") {
      withDirectory(path: "/src", directory:"$SOURCE") {
        withWorkdir(path:"/src/app/frontend") {
          withExec(args:["go", "build", "-o", "$APP_NAME-frontend", "main.go"]) {
            file(path:"$APP_NAME-frontend") {
              export(path:"output/$APP_NAME-frontend")
            }
          }
        }
      }
    }
  }
}
EOF
)

# check build result and display message
if [ "$BUILD_BACKEND" == "true" ]
then
    echo "Build backend successful"
else
    echo "Build backend unsuccessful"
fi

# check build result and display message
if [ "$BUILD_API" == "true" ]
then
    echo "Build api successful"
else
    echo "Build api unsuccessful"
fi

# check build result and display message
if [ "$BUILD_FRONTEND" == "true" ]
then
    echo "Build frontend successful"
else
    echo "Build frontend unsuccessful"
fi