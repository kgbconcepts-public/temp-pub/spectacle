package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Define a struct to hold the data retrieved from the HTTP endpoint
type Info struct {
	Name        string                 `json:"name"`
	Description string                 `json:"description"`
	Additional  map[string]interface{} `json:"additional,omitempty"` // Optional field for additional data
}

func getInfoFromHTTP(url string) (*Info, error) {
	// Make an HTTP GET request to the endpoint
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("error getting information: %w", err)
	}

	defer resp.Body.Close()

	// Check for successful response
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	// Decode the JSON response body into the Info struct
	var info Info
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&info)
	if err != nil {
		return nil, fmt.Errorf("error decoding JSON: %w", err)
	}

	return &info, nil
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	// Replace with your actual logic to determine endpoint URL
	url := "https://catfact.ninja/fact" // Placeholder

	info, err := getInfoFromHTTP(url)
	if err != nil {
		// Handle error appropriately, e.g., return error message with specific status code
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Marshal the Info struct back to JSON
	jsonData, err := json.Marshal(info)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Set content type and write JSON response
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}

func main() {
	// **Handler registration for the standard library's http package**
	http.HandleFunc("/", handleRequest)

	fmt.Println("Server listening on port 8080")
	http.ListenAndServe(":8080", nil)
}
